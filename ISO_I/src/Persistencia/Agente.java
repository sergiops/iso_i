package Persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.List;
import java.sql.*;
import java.util.LinkedList;
import java.util.Vector;

public class Agente {
	
	//instancia del agente
    protected static Agente mInstancia=null;
    //Conexion con la base de datos
    protected static Connection mBD;
	//Identificador ODBC de la base de datos
    private static String url="jdbc:odbc:ISO";
    //Driven para conectar con bases de datos Microsoft Access 
    private static String driver="sun.jdbc.odbc.JdbcOdbcDriver";
    

	private Agente() throws Exception{
		//throw new UnsupportedOperationException();
		conectar();
	}
	
	public static Agente getAgente() throws Exception{
		//throw new UnsupportedOperationException();
		if (mInstancia==null){
	          mInstancia=new Agente();
	        }
	        return mInstancia;
	}
	
	private void conectar() throws Exception {
        Class.forName(driver);
        mBD=DriverManager.getConnection(url);
   }


	public void desconectar() throws Exception{
    	mBD.close();
    }
	

    public int insert(String SQL) throws SQLException, Exception{ 
     	conectar();
    	PreparedStatement stmt = mBD.prepareStatement(SQL);
    	int res=stmt.executeUpdate();
    	stmt.close();
    	desconectar();
    	return res;
    }
    
    public void delete(String nombre) throws SQLException,Exception{
    	String n;
		Connection conexion;
		Statement sentencia;
		ResultSet resultado;
		String SQL=null;
		
		conexion=DriverManager.getConnection(url);
		sentencia=conexion.createStatement();
			resultado =sentencia.executeQuery("SELECT * FROM Pelicula");
			while(resultado.next()){
				conexion=DriverManager.getConnection(url);
				sentencia=conexion.createStatement();
				n=resultado.getString ("Nombre");
				if(nombre.equalsIgnoreCase(n)){
					SQL = "DELETE FROM Pelicula WHERE Nombre = '"+n+"';";
					sentencia.execute(SQL);
				}	
			}
			sentencia.close();
			conexion.close();
	
    }
    
    
    public int update(String SQL) throws SQLException,Exception{
    	conectar();
    	PreparedStatement stmt = mBD.prepareStatement(SQL);
    	int res=stmt.executeUpdate();
    	stmt.close();
    	desconectar();
    	return res;
    }



    public Vector<Object> select(String SQL) throws SQLException,Exception{
	//Creamos el 'vector de vectores'
        Vector resultado = new Vector();
        conectar();
        
        Statement sentencia = mBD.createStatement();
        ResultSet resultQuery = sentencia.executeQuery(SQL);
        String login, password;
        while(resultQuery.next()){
            Vector u = new Vector();
            login = resultQuery.getString("login");
            password = resultQuery.getString("password");
            u.add(login);
            u.add(password);
            
            resultado.add(u);
        }
        
        return resultado;
    }
	
}