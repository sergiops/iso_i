package Interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import Dominio.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaA�adirPelicula extends JFrame {

	private JPanel contentPane;
	private JTextField Nombre;
	private JTextField textField;
	private JTextField Genero;
	private JTextField Director;
	private JTextField A�o;
	private JTextField Sinopsis;
	private JTextField Reparto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaA�adirPelicula frame = new VistaA�adirPelicula();
					frame.setVisible(true);
					frame.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaA�adirPelicula() throws Exception {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Nombre = new JTextField();
		Nombre.setBounds(178, 40, 147, 20);
		Nombre.setText("");
		contentPane.add(Nombre);
		Nombre.setColumns(10);
		
		JLabel Nombre_et = new JLabel("Nombre de la Pelicula");
		Nombre_et.setBounds(24, 43, 144, 14);
		contentPane.add(Nombre_et);
		
		textField = new JTextField();
		textField.setBounds(344, 156, -221, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblGnero = new JLabel("G\u00E9nero");
		lblGnero.setBounds(24, 68, 147, 14);
		contentPane.add(lblGnero);
		
		JLabel lblDirector = new JLabel("Director");
		lblDirector.setBounds(24, 93, 144, 14);
		contentPane.add(lblDirector);
		
		JLabel lblAo = new JLabel("A\u00F1o");
		lblAo.setBounds(24, 119, 144, 14);
		contentPane.add(lblAo);
		
		JLabel lblSinopsis = new JLabel("Sinopsis");
		lblSinopsis.setBounds(24, 144, 144, 14);
		contentPane.add(lblSinopsis);
		
		JLabel lblReparto = new JLabel("Reparto");
		lblReparto.setBounds(24, 169, 144, 14);
		contentPane.add(lblReparto);
		
		Genero = new JTextField();
		Genero.setBounds(178, 65, 147, 20);
		contentPane.add(Genero);
		Genero.setColumns(10);
		
		Director = new JTextField();
		Director.setBounds(178, 90, 147, 20);
		contentPane.add(Director);
		Director.setColumns(10);
		
		A�o = new JTextField();
		A�o.setBounds(178, 116, 147, 20);
		contentPane.add(A�o);
		A�o.setColumns(10);
		
		Sinopsis = new JTextField();
		Sinopsis.setBounds(178, 141, 147, 20);
		contentPane.add(Sinopsis);
		Sinopsis.setColumns(10);
		
		Reparto = new JTextField();
		Reparto.setBounds(178, 166, 147, 20);
		contentPane.add(Reparto);
		Reparto.setColumns(10);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				int n;
				GestorProveedores p= new GestorProveedores();
				try {
					n=p.a�adirPelicula(Nombre.getText(), Genero.getText(), Director.getText(),A�o.getText(), Sinopsis.getText(), Reparto.getText());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				Nombre.setText("");
				Genero.setText("");
				Director.setText("");
				A�o.setText("");
				Sinopsis.setText("");
				Reparto.setText("");
			
				
			}
		});
		btnGuardar.setBounds(79, 227, 89, 23);
		contentPane.add(btnGuardar);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnVolver.setBounds(236, 227, 89, 23);
		contentPane.add(btnVolver);
	}
}
