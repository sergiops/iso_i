package Interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.util.Date;

import Dominio.*;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Checkbox;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;

public class VistaProveedor extends JFrame {

		private GestorProveedores p;
		private JPanel contentPane;
		private JLabel lblSeHaAutenticado;
		private final Action action = new SwingAction();

		/**
		 * 
		 * @param estadoPelicula
		 */
		private void mostrarMensajes(String estadoPelicula) {
			throw new UnsupportedOperationException();
		}

		/**
		 * 
		 * @param nombre
		 * @param genero
		 * @param director
		 * @param anio
		 * @param sinopsis
		 * @param reparto
		 */
		protected void añadirPelicula(String nombre, String genero, String director, Date anio, String sinopsis, String reparto) {
			throw new UnsupportedOperationException();
		}

		/**
		 * 
		 * @param nombre
		 * @param anio
		 */
		protected void eliminarPelicula(String nombre, Date anio) {
			throw new UnsupportedOperationException();
		}



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaProveedor frame = new VistaProveedor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaProveedor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblSeHaAutenticado = new JLabel("Se ha autenticado como Proveedor");
		lblSeHaAutenticado.setBounds(89, 41, 223, 14);
		contentPane.add(lblSeHaAutenticado);
		
		JRadioButton rdbtnAddPelicula = new JRadioButton("A\u00F1adir Pelicula");
		rdbtnAddPelicula.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				VistaAñadirPelicula ap;
				try {
					ap = new VistaAñadirPelicula();
					ap.setVisible(true);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
		});
		rdbtnAddPelicula.setBounds(125, 93, 165, 23);
		contentPane.add(rdbtnAddPelicula);
		
		JRadioButton rdbtnModificarPelicula = new JRadioButton("Modificar Pelicula");
		rdbtnModificarPelicula.setBounds(125, 119, 165, 23);
		contentPane.add(rdbtnModificarPelicula);
		
		JRadioButton rdbtnEliminarPelicula = new JRadioButton("Eliminar Pelicula");
		rdbtnEliminarPelicula.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				
				VistaEliminarPelicula ep= new VistaEliminarPelicula();
				ep.setVisible(true);
				
			}
		});
		rdbtnEliminarPelicula.setBounds(125, 145, 165, 23);
		contentPane.add(rdbtnEliminarPelicula);
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
