package Interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import Dominio.*;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaEliminarPelicula extends JFrame {

	private JPanel contentPane;
	private JTextField textNombre;
	private JButton btnVolver;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaEliminarPelicula frame = new VistaEliminarPelicula();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaEliminarPelicula() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 439, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textNombre = new JTextField();
		textNombre.setBounds(172, 77, 238, 20);
		contentPane.add(textNombre);
		textNombre.setColumns(10);
		
		
		JLabel lblNombreDeLa = new JLabel("Nombre de la Pelicula");
		lblNombreDeLa.setBounds(22, 76, 196, 23);
		contentPane.add(lblNombreDeLa);
		
		JButton btnEliminarPelcula = new JButton("Eliminar Pel\u00EDcula");
		btnEliminarPelcula.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				
				GestorProveedores eliminar=new GestorProveedores();
				eliminar.eliminarPelicula(textNombre.getText());
				textNombre.setText("");
				System.exit(1);
			}
		});
		
		btnEliminarPelcula.setBounds(22, 189, 166, 23);
		contentPane.add(btnEliminarPelcula);
		
		btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnVolver.setBounds(255, 189, 89, 23);
		contentPane.add(btnVolver);

	}
}
